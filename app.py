from flask import Flask, render_template, redirect, request
from flask_sqlalchemy import SQLAlchemy
# from app import db
import datetime
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///e.db'
db = SQLAlchemy(app)

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True, nullable=False)
    subtitle = db.Column(db.String(80), nullable=False)
    desc = db.Column(db.String, nullable=False)
    author = db.Column(db.String, nullable=False)
    date = datetime.datetime.now()

    def __repr__(self):
        return '<Post %r>' % self.title

class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(80), nullable=False)
    phone = db.Column(db.String, nullable=False)
    message = db.Column(db.String, nullable=False)
    date = datetime.datetime.now()

    def __repr__(self):
        return '<Contact %r>' % self.name
db.create_all()

db = SQLAlchemy(app)
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/post')
def post():
    return render_template('post.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/addcontact',methods=['GET','POST'])
def addcontact():
    name = request.form.get('name')
    email = request.form['email']
    phone = request.form['phone']
    message = request.form['message']
    data = Contact(name=name, email=email, phone=phone, message=message)
    print(data)
    db.session.add(data)
    db.session.commit()
    return redirect('/contact')

@app.route('/about')
def about():
    return render_template('about.html')

if __name__ == "__main__":
    app.run(debug=True)